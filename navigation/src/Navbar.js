import './App.css';
import { useState } from 'react';
import {NavLink, Route, Routes} from "react-router-dom"
import {Login} from "./pages/Login"
import {Food} from "./pages/Food"
import {Stuff} from "./pages/Stuff"
import{Item} from "./pages/Item"
import{Logout} from"./pages/Logout"
import ProtectedRoutes from './hooks/ProtectedRoutes';


export default function Navbar(){
  const [auth, setAuth] = useState(false);


  const login = () => {
      setAuth(true);
      console.log("Does This Work");
  }

 const logout = () => {
      setAuth(false);
  }

    return(
        <>
      <nav className = "nav">
        <h1 className = "site-title"> CompanyLogo</h1>
        {
          auth ?
          <ul>
          <li><NavLink to ="/food"> Food </NavLink></li>
          <li><NavLink to ="/stuff"> Stuff </NavLink></li>
          <li><NavLink  onClick ={logout} to ="/logout"> Logout </NavLink></li>
          </ul>
          :
          //if false, show login tab only.
          <ul>
           <li><NavLink to ="/food"replace onClick= {login}> Login </NavLink></li>
          </ul>
        }
        
        

      </nav>
      <Routes >
        <Route path="/" element ={<Login />} />
        <Route path="/logout" element ={<Logout />} />
        <Route path="/food" element ={<Food />} />
        <Route path="/stuff" element ={<Stuff/>} />
        <Route path="/stuff/:item" element ={<Item/>} />
        <Route path="*" element ={<div> 404 Not Found</div>} />
      </Routes>
       </>
      )
    
}