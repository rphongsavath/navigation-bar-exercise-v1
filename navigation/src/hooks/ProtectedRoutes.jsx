
import {Navigate, Outlet} from "react-router-dom"
import{useLocation} from "react-router"
import { Login } from "../pages/Login";

const useAuth = () => {
    const user = {isLoggedIn: false };
    return user && user.isloggedIn;
};

const ProtectedRoutes = () =>{
    const isAuth = useAuth();
    return isAuth ? <Outlet/> : <Navigate to='/' />

};

export default ProtectedRoutes;