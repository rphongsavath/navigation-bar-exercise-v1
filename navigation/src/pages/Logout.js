import {useEffect}from 'react'
import { useNavigate} from 'react-router-dom';


export  function Logout({logout}) {
    let navigate = useNavigate();
     // logout();

useEffect(() => {
    //set async of 2 second delay to display page
    setTimeout(() =>{
      
      navigate('/')  
    },2000)
})   

  return (
 
    <div>
       <h1 className ="titleText">Logout Page</h1> 
       <p className ="bobyText">Please wait while processing request</p>
    </div>
  )
}
