import { useNavigate} from "react-router-dom";
export function Stuff() {

    const itemListing= [
        {id: 1,"src": "img/axe.jpg", "alt":"Axe", "price": 25},
        {id:2, "src": "img/hammer.jpg", "alt":"Hammer", "price": 15},
        {id:3,"src": "img/HolePuncher.jpg", "alt":"Hole Puncher", "price": 2},
        {id:4,"src": "img/PaintRoll.jpg", "alt":"Paint Roller", "price": 12},
        {id:5,"src": "img/pencil.jpg", "alt":"Pencil", "price": 1},
        {id:6,"src": "img/Screwdriver (2).jpg", "alt":"Screwdrivers", "price": 35},
        {id:7,"src": "img/screwdriver.jpg", "alt":"Screwdrivers", "price": 16},
        {id:8,"src": "img/shears.jpg", "alt":"Shears", "price": 5},
        {id:9,"src": "img/SkinCare.jpg", "alt":"Skin Care Products", "price": 125},
        {id:10,"src": "img/Tweezers.jpg", "alt":"Tweezers", "price": 30}

    ]

    const navigate = useNavigate();

    const handleItemClick = (item) =>{
        navigate('/stuff/${item.id}',{state:item.alt} );
    }

    return(
      <div> 
        <div>
        <h1 className ="titleText">Stuff Department Page</h1>
        <p className ="bobyText">We are about much more than just tools. We're about the opportunities tools represent ‐ the projects, the builds, the problem solving, the creativity, and the connection they give us to others.</p>

        </div>
        <h2>Item Section</h2>
        <div className = "stuffDisplay">
           { itemListing.map((item) => (
                
                <img 
                    key={item.id}
                    src ={item.src} 
                    alt={item.alt} 
                    className = "itemPicture" 
                    onClick = {() =>handleItemClick(item)}
                />

            ))}
        </div>
      </div>   
    ) 

}