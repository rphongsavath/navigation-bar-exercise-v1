## Project Name: 
Navigation Bar Exercise v1

## Project Decription: 
This application displays a series of webpages for users to view and interact. The application starts with the user logging in. Once the user is logged in, the user has a new navigation bar for pages to select.

## Deployment
1. Copy the application from the GitLab repository
2. Navigate to the application folder
3. Right click in the folder and select "Git Bash Here"
4. Navigate up one folder to the src folder
5. Use npm install to download the necessary depenencies to run app
6. Type 'npm start' in the Git Bash terminal and application should start local port 3000 in a seperate browser window. If port is already in use, type 'npx kill-port 3000' in the Git Bash terminal. Rerun 'npm start'.
7. Open application folder in Visual Studio to review code. 


## Author: Richy Phongsavath
